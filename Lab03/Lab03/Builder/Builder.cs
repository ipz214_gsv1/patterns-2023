﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03.Builder
{
    public interface IHeroBuilder
    {
        IHeroBuilder BuildHeight();

        IHeroBuilder BuildWeight();

        IHeroBuilder BuildHairColor();

        IHeroBuilder BuildEyeColor();

        IHeroBuilder BuildHeroName();

        Hero GetHero();
    }

    public class Hero
    {
        private int height;

        private int weight;

        private string hairColor;

        private string eyeColor;

        private string name;

        public int SetHeight(int height)
        {
           return this.height = height;
        }

        public int SetWeight(int weight)
        {
            return this.weight = weight;
        }

        public string SetHairColor(string hairColor)
        {
            return this.hairColor = hairColor;
        }

        public string SetEyeColor(string eyeColor)
        {
            return this.eyeColor = eyeColor;
        }

        public string SetName(string name)
        {
            return this.name = name;
        }
    }

    class EnemyBuilder : IHeroBuilder
    {
        private Hero hero = new Hero();

        private void Reset()
        {
            this.hero = new Hero();
        }

        public Hero GetHero()
        {
            Hero hero = this.hero;

            this.Reset();

            return hero;
        }

        public IHeroBuilder BuildEyeColor()
        {
            this.hero.SetEyeColor("Green");

            return this;
        }

        public IHeroBuilder BuildHairColor()
        {
            this.hero.SetHairColor("White");

            return this;
        }

        public IHeroBuilder BuildHeight()
        {
            this.hero.SetHeight(150);

            return this;
        }

        public IHeroBuilder BuildHeroName()
        {
            this.hero.SetName("Enemy");

            return this;
        }

        public IHeroBuilder BuildWeight()
        {
            this.hero.SetWeight(70);

            return this;
        }
    }
    class HeroBuilder : IHeroBuilder
    {
        private Hero hero = new Hero();

        private void Reset()
        {
            this.hero = new Hero();
        }

        public Hero GetHero()
        {
            Hero hero = this.hero;

            this.Reset();

            return hero;
        }

        public IHeroBuilder BuildHeight()
        {
            this.hero.SetHeight(200);

            return this;
        }

        public IHeroBuilder BuildWeight()
        {
            this.hero.SetWeight(100);

            return this;
        }

        public IHeroBuilder BuildHairColor()
        {
            this.hero.SetHairColor("Black");

            return this;
        }

        public IHeroBuilder BuildEyeColor()
        {
            this.hero.SetEyeColor("brown");

            return this;
        }

        public IHeroBuilder BuildHeroName()
        {
            this.hero.SetName("Hero");

            return this;
        }
    }
}
