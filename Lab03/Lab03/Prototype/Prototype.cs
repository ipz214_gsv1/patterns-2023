﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03.Prototype
{
    public class Virus
    {
        public int Weight { get; set; }

        public int Age { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public List<VirusChild> Children { get; set; }

        public Virus(int weight, int age, string name, string type, List<VirusChild> children)
        {
            Weight = weight;
            Age = age;
            Name = name;
            Type = type;
            Children = children;
        }

        public virtual Virus Clone() => this;
    }

    public class VirusChild : ICloneable
    {
        public DateTime BirthDate { get; set; }

        public Virus Parent { get; set; }

        public VirusChild(DateTime birthDate, Virus parent)
        {
            BirthDate = birthDate;
            Parent = parent;
        }

        public object Clone() => new VirusChild(this.BirthDate, this.Parent);
    }
}
