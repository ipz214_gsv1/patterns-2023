﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03.Singleton
{
    public sealed class DatabaseRepository
    {
        private static DatabaseRepository instance;

        private static readonly object padlock = new object();

        private DatabaseRepository() { }

        public static DatabaseRepository GetInstance()
        {
            lock (padlock)
            {
                if (instance == null)
                    instance = new DatabaseRepository();
             
                return instance;
            }
        }
    }
}
