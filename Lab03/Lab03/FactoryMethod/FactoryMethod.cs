﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03.FactoryMethod
{
    public abstract class Subscription
    {
        public decimal MonthlyFee { get; set; }

        public int MinSubscriptionPeriod { get; set; }

        public List<string> Channels { get; set; }
    }

    public class DomesticSubscription : Subscription
    {
        public DomesticSubscription() 
        {
            MonthlyFee = 100;
            MinSubscriptionPeriod = 3;
            Channels = new List<string>();
        }
    }


    public class EducationalSubscription : Subscription
    {
        public EducationalSubscription()
        {
            MonthlyFee = 150;
            MinSubscriptionPeriod = 8;
            Channels = new List<string>();
        }
    }

    public class PremiumSuscription : Subscription
    {
        public PremiumSuscription()
        {
            MonthlyFee = 170;
            MinSubscriptionPeriod = 10;
            Channels = new List<string>();
        }
    }

    public interface ISubscriptionFactory
    {
        Subscription CreateSubscription();
    }

    public class WebsiteSubscriptionFactory : ISubscriptionFactory
    {
        public Subscription CreateSubscription() => new DomesticSubscription();
    }

    public class MobileAppSubscriptionFactory : ISubscriptionFactory
    {
        public Subscription CreateSubscription() => new EducationalSubscription();
    }

    public class ManagerCallSubscriptionFactory : ISubscriptionFactory
    {
        public Subscription CreateSubscription() => new PremiumSuscription();
    }

    public class SubscriptionManager
    {
        private ISubscriptionFactory factory;

        public SubscriptionManager(ISubscriptionFactory factory)
        {
            this.factory = factory;
        }

        public Subscription PurchaseSubscription()
        {
            return factory.CreateSubscription();
        }
    }
}
