﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03.AbstractFactory
{
    interface IShoes { }
    interface IShirt { }

    class WomenShirt : IShirt { }
    class MenShirt : IShirt { }
    class KidShirt : IShirt { }

    class WomenShoes : IShoes { }
    class MenShoes : IShoes { }
    class KidShoes : IShoes { }

    interface IClothingFactory
    {
        IShoes CreateShoes();

        IShirt CreateShirt();
    }

    class WomenClothingFactory : IClothingFactory
    {
        public IShirt CreateShirt() => new WomenShirt();

        public IShoes CreateShoes() => new WomenShoes();
    }

    class MenClothingFactory : IClothingFactory
    {
        public IShirt CreateShirt() => new MenShirt();

        public IShoes CreateShoes() => new MenShoes();
    }

    class KidClothingFactory : IClothingFactory
    {
        public IShirt CreateShirt() => new KidShirt();

        public IShoes CreateShoes() => new KidShoes();
    }
}
