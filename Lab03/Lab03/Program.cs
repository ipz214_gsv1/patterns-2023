﻿
using Lab03.AbstractFactory;
using Lab03.Builder;
using Lab03.FactoryMethod;
using Lab03.Prototype;
using Lab03.Singleton;

//Factory method

SubscriptionManager manager1 = new SubscriptionManager(new WebsiteSubscriptionFactory());

Subscription subscription = manager1.PurchaseSubscription();

Console.WriteLine("MonthlyFee: ", subscription.MonthlyFee);
Console.WriteLine("MinSubscriptionPeriod: ", subscription.MinSubscriptionPeriod);

SubscriptionManager manager2 = new SubscriptionManager(new MobileAppSubscriptionFactory());

Subscription subscription1 = manager2.PurchaseSubscription();

Console.WriteLine("MonthlyFee: ", subscription1.MonthlyFee);
Console.WriteLine("MinSubscriptionPeriod: ", subscription1.MinSubscriptionPeriod);

//Abstract Factory

IClothingFactory factory = new WomenClothingFactory();

var shirt = factory.CreateShirt();
var shoes = factory.CreateShoes();

//Singleton

var repository = DatabaseRepository.GetInstance();

//Builder

var builder = new HeroBuilder()
    .BuildHairColor()
    .BuildHeroName()
    .BuildEyeColor();

var hero = builder.GetHero();

var builder2 = new EnemyBuilder()
    .BuildHairColor()
    .BuildHeroName()
    .BuildEyeColor();

var enemy = builder2.GetHero();


//Prototype
var virus1 = new Virus(100, 10, "COVID-19", "type", children1);

var children1 = new List<VirusChild>()
{
    new VirusChild(DateTime.Now.AddDays(10), virus1),
};

Virus clonedVirus1 = virus1.Clone();

var virus2 = new Virus(120, 10, "COVID-20", "type2", children2);

var children2 = new List<VirusChild>()
{
    new VirusChild(DateTime.Now.AddDays(10), virus2),
};

Virus clonedVirus2 = virus2.Clone();



