﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public abstract class Dish
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Calories { get; set; }

        public abstract Dish CookDish();
    }

    public class Salad : Dish
    {
        public override Dish CookDish()
        {
            return new Salad();
        }
    }

    public class Cake : Dish
    {
        public override Dish CookDish()
        {
            return new Cake();
        }
    }

    public class Milk : Dish
    {
        public override Dish CookDish()
        {
            return new Milk();
        }
    }

    public class Water : Dish
    {
        public override Dish CookDish()
        {
            return new Water();
        }
    }

    public abstract class ServiceStep
    {
        public abstract ServiceStep CompleteServiceStep();
    }

    public class Package : ServiceStep
    {
        public override ServiceStep CompleteServiceStep()
        {
            return new Package();
        }
    }

    public class Napkin : ServiceStep
    {
        public override ServiceStep CompleteServiceStep()
        {
            return new Napkin();
        }
    }

    public class Price : ServiceStep
    {
        public override ServiceStep CompleteServiceStep()
        {
            return new Price();
        }
    }

    public class BigMacMenuFacade
    {
        private Salad salad;
        private Cake cake;
        private Milk milk;
        private Water water;
        private Package package;
        private Napkin napkin;
        private Price price;

        public BigMacMenuFacade()
        {
            salad = new Salad();
            cake = new Cake();
            milk = new Milk();
            water = new Water();
            package = new Package();
            napkin = new Napkin();
            price = new Price();
        }
        
        public void CookFood()
        {
            var salad = this.salad.CookDish();
            var cake = this.cake.CookDish();
            var milk = this.milk.CookDish();
            var water = this.water.CookDish();
            var package = this.package.CompleteServiceStep();
            var napkin = this.package.CompleteServiceStep();
            var price = this.price.CompleteServiceStep();
        }
    }
}
