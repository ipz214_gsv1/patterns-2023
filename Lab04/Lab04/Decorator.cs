﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public abstract class Hero
    {
       public List<Item> Inventory = new List<Item>();
       public abstract string GetName();

       public void AddItem(Item item)
       {
            Inventory.Add(item);
       }

        public void RemoveItem(Item item) 
        {
            Inventory.Remove(item);
        }
    }

    public class Warrior : Hero
    {
        public string GetName()
        {
            throw new NotImplementedException();
        }
    }

    public class Mage : Hero
    {
        public string GetName()
        {
            throw new NotImplementedException();
        }
    }

    public class Palladin : Hero
    {
        public string GetName()
        {
            throw new NotImplementedException();
        }
    }

    public class Item
    {
        protected string name;

        public Item(string name)
        {
            this.name = name;
        }
    }

    public class ClotingDecorator : Item
    {
        private readonly Item item;

        public ClotingDecorator(Item item, string name) : base(name) 
        {
            this.item = item;
        }
    }

    public class WeaponDecorator : Item
    {
        private readonly Item item;

        public WeaponDecorator(Item item, string name) : base(name)
        {
            this.item = item;
        }
    }

    public class ArtefactsDecorator : Item
    {
        private readonly Item item;

        public ArtefactsDecorator(Item item, string name) : base(name)
        {
            this.item = item;
        }
    }
}
