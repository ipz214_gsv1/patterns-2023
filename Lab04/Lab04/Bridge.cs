﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public abstract class Shape
    {
        protected readonly IDrawApi drawApi;
        public Shape(IDrawApi drawApi)
        {
            this.drawApi = drawApi;
        }

        public abstract void Draw();
    }

    public class Circle : Shape
    {
        public override void Draw()
        {
            throw new NotImplementedException();
        }
    }

    public class Square : Shape
    {
        public override void Draw()
        {
            throw new NotImplementedException();
        }
    }

    public class Triangle : Shape
    {
        public override void Draw()
        {
            throw new NotImplementedException();
        }
    }

    public interface IDrawApi
    {
        void DrawCircle(int x, int y, int radius);
        void DrawSquare(int x, int y, int side);
        void DrawTriangle(int x1, int y1, int x2, int y2, int x3, int y3);
    }

    class VectorDrawingAPI : IDrawApi
    {
        public void DrawCircle(int x, int y, int radius)
        {
            Console.WriteLine("as vector");
        }
        public void DrawSquare(int x, int y, int side)
        {
            Console.WriteLine("as vector");
        }
        public void DrawTriangle(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            Console.WriteLine("as vector");
        }
    }

    // Клас, який рендерить растрову графіку
    class RasterDrawingAPI : IDrawApi
    {
        public void DrawCircle(int x, int y, int radius)
        {
            Console.WriteLine("as pixels");
        }
        public void DrawSquare(int x, int y, int side)
        {
            Console.WriteLine("as pixels");
        }
        public void DrawTriangle(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            Console.WriteLine("as pixels");
        }
    }
}
