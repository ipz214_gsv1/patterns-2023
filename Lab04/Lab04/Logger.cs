﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public interface ILogger
    {
        void Log(string message);

        void Warn(string message);

        void Error(string message);
    }

    public class ConsoleLogger : ILogger
    {
        public void Log(string message) => Console.WriteLine(message, ConsoleColor.Green);

        public void Warn(string message) => Console.WriteLine(message, ConsoleColor.Yellow);

        public void Error(string message) => Console.WriteLine(message, ConsoleColor.Red);
    }

    public class FileWriter
    {
        private readonly string filename;

        public FileWriter(string filename)
        {
            this.filename = filename;
        }

        public void Write(string message)
        {
            using(var stream = new StreamWriter(filename))
            {
                stream.Write(message);
            }
        }

        public void WriteLine(string message)
        {
            using(var stream = new StreamWriter(filename))
            {
                stream.WriteLine(message);
            }
        }
    }

    public class FileLoggerAdapter : ILogger
    {
        private readonly FileWriter fileWriter;

        public FileLoggerAdapter(FileWriter fileWriter)
        {
            this.fileWriter = fileWriter;
        }

        public void Error(string message)
        {
            fileWriter.WriteLine(message);
        }

        public void Log(string message)
        {
            fileWriter.WriteLine(message);
        }

        public void Warn(string message)
        {
            fileWriter.WriteLine(message);
        }
    }
}
